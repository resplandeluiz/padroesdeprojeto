<?php
    
    require 'Investimento.php';
    require 'Conta.php';
    require 'CalcularInvestimento.php';
    require 'Conservador.php';
    require 'Moderado.php';
    require 'Arrojado.php';
    
    $conta = new Conta(500);
  
    $calcular = new CalcularInvestimento();

    echo $calcular->realizarInvestimento($conta, new Conservador());
   
    echo '<br/>';
    
    echo $calcular->realizarInvestimento($conta, new Moderado());
    
     echo '<br/>';
    
    echo $calcular->realizarInvestimento($conta, new Arrojado());
    



?>