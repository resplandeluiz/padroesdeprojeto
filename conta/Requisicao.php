<?php

        class Requisicao{
                
                
                private $formato;
            
                function __construct($formato){ 
                        
                        $this->formato = $formato; 
                        
                }
                
                public function getFormato(){ 
                        
                        return $this->formato; 
                        
                }
                
                public function responde(Requisicao $req, Conta $conta){
            
                    $xml = new XML();
                    $porcento = new Porcento();
                    $csv = new CSV();
                    
                    $xml->setProxima($porcento);
                    $porcento->setProxima($csv);
                   
                    
                    $xml->responde($req, $conta);
                   
                }
            
            
            
        }

?>