<?php


    class CSV implements Resposta{
        
        
         private $proximaResposta;
            
               
      
        
            public function responde(Requisicao $req, Conta $conta){
                
              
               if( $req->getFormato() == 'CSV'){
                   
                    echo $conta->getNome().','.$conta->getSaldo();
                   
               }else{
                   
                   return $this->proximaResposta->responde($req,$conta);
               }
                
                
            }
            
            public function getProxima(){
                
                return $this->proximaResposta;
                
            }
            
            public function setProxima(Resposta $resposta){
                
                $this->proximaResposta = $resposta;
                
            }
        
        
    }

?>