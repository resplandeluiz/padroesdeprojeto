<?php

    class Porcento implements Resposta{
        
            private $proximaResposta;
            
           
        
            public function responde(Requisicao $req, Conta $conta){
                
              
               if( $req->getFormato() == '%'){
                   
                    echo '%'.$conta->getNome().'%'.$conta->getSaldo().'%';
                   
               }else{
                   
                   return $this->proximaResposta->responde($req,$conta);
               }
                
                
            }
            
            public function setProxima(Resposta $resposta){
                
                $this->proximaResposta = $resposta;
                
            }
        
        
        
    }


?>