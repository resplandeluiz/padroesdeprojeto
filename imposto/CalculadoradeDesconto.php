<?php

    class CalculadoradeDesconto{
    
        public function desconto(Orcamento $orcamento){
            
            $desconto5Itens = new Desconto5Itens();
            $desconto500Reais = new Desconto500Reais();
            $descontoCasado = new DescontoCasado();
            $semDesconto = new SemDesconto();
            
            $desconto5Itens->setProximo($desconto500Reais);
            $desconto500Reais->setProximo($descontoCasado);
            $descontoCasado->setProximo($semDesconto);
            
            $valorDodesconto = $desconto5Itens->desconto($orcamento);
            return $valorDodesconto;
        }
    
    }
    
?>