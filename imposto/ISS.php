<?php

    class ISS  extends TemplateImposto{
        
     protected function deveUsarOMaximo(Orcamento $orcamento){
            
            return $orcamento->getValor() > 300;
            
        }
        
        protected function taxacaoMinima(Orcamento $orcamento){
            
             return $orcamento->getValor() * 0.1;
        }
        
        protected function taxacaoMaxima(Orcamento $orcamento){
            
             return $orcamento->getValor() * 0.15;
            
        }
        
    }


?>