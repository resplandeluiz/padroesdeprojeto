<?php



    class ImpostoY extends TemplateDeImpostoCondicional implements Imposto {

      public function deveUsarMaximaTaxacao(Orcamento $orcamento) {
        return $orcamento->getValor() > 500 && $this->temItemMaiorQue100ReaisNo($orcamento);
      }
      public function maximaTaxacao(Orcamento $orcamento) { 
        return $orcamento->getValor() * 0.10;  
      }
      public function minimaTaxacao(Orcamento $orcamento) {
        return $orcamento->getValor() * 0.06;
      }

      private function temItemMaiorQue100ReaisNo(Orcamento $orcamento) {
          
        return true;
      }
    }


?>