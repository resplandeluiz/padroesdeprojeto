<?php


    class IHIT extends TemplateImposto{
        
        
        
         protected function deveUsarOMaximo(Orcamento $orcamento){
            
            return $this->temComMesmoNome($orcamento);
            
        }
        
        protected function taxacaoMinima(Orcamento $orcamento){
            
            return count($orcamento->getItens()) * 0.01;
        }
        
        protected function taxacaoMaxima(Orcamento $orcamento){
            
             return $orcamento->getValor() * 0.13 + 100;
            
        }
         
        private function temComMesmoNome(Orcamento $orcamento) {
              
            foreach($orcamento->getItens() as $item) {
               
                foreach($orcamento->getItens() as $item_2){
                    
                    if($item->getNome() == $item_2->getNome()){
                        
                        return true;
                    }
                    
                    
                }
              
                
             
            }
            
            return false;
            
        }
        
        
        
    }



?>