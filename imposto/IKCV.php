<?php

    class IKCV extends TemplateImposto {
        
        
         protected function deveUsarOMaximo(Orcamento $orcamento){
            
            return $orcamento->getValor() > 500 && $this->temItemMaiorQue100Reais($orcamento);
            
        }
        
        protected function taxacaoMinima(Orcamento $orcamento){
            
             return $orcamento->getValor() * 0.06;
        }
        
        protected function taxacaoMaxima(Orcamento $orcamento){
            
             return $orcamento->getValor() * 0.10;
            
        }
         
        private function temItemMaiorQue100Reais(Orcamento $orcamento) {
              
            foreach($orcamento->getItens() as $item) {
              
                if($item->getValor() > 100) return true;
             
            }
            
            return false;
            
        }
        
        
        
    }


?>