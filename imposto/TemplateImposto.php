<?php


    abstract class TemplateImposto implements Imposto{
        
        
        public function calculaImposto(Orcamento $orcamento){
            
            if( $this->deveUsarOMaximo($orcamento) ){
                
                return $this->taxacaoMaxima($orcamento);
                
            }else{
                
                return $this->taxacaoMinima($orcamento);
                
            }
            
            
        }
        
        
        protected abstract function deveUsarOMaximo(Orcamento $orcamento);
        protected abstract function taxacaoMinima(Orcamento $orcamento);
        protected abstract function taxacaoMaxima(Orcamento $orcamento);
        
        
    }
        
        
        
    
?>